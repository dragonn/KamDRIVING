/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */






#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/flash.h>



#include <libopencm3/cm3/nvic.h>
//#include <libopencm3/stm32/flash.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/timer.h>

#include "lib/ssi.h"
#include "lib/encoders.h"
#include "lib/clock.h"
#include "lib/pwm.h"
#include "lib/pid.h"
#include "lib/dir.h"
#include "lib/ab.h"
#include "lib/uart.h"
#include "lib/wathdog.h"
#include "lib/rtc.h"
#include "lib/flash.h"

void sys_tick_handler(void) {
	pwm_calc(&pwm1);
	pwm_calc(&pwm2);
	pwm_calc(&pwm3);
	pwm_calc(&pwm4);
	pwm_calc(&pwm5);
	wathdog_reset();
}

#define APP_ADDRESS	0x08000000
void hard_fault_handler(void) {
	/*if ((*(volatile uint32_t *)APP_ADDRESS & 0x2FFE0000) == 0x20000000) {
		// Set vector table base address. 
		SCB_VTOR = APP_ADDRESS & 0xFFFF;
		// Initialise master stack pointer. 
		__asm__ volatile("msr msp, %0"::"g"
					(*(volatile uint32_t *)APP_ADDRESS));
		// Jump to application. 
		(*(void (**)())(APP_ADDRESS + 4))();
	}
	while(1);*/
}
uint32_t rtccount = 0;
int main(void) {
	clock_init();
	rtc_init();
	pwm_init();
	dir_init();
	
	encoders_init();

	uint16_t ssi1, ssi2, ssi3, ssi4;
	ssi_read(&ssi1, &ssi2, &ssi3, &ssi4);

	ab_init();
	ab_set(ssi1, ssi2, ssi3, ssi4);


	
	//to-test
	pwm_init();
	//to-test
	
	//to-test
	pid_init();
	pid_read();
	//to-do, not working
	
	pwm1.set=0;
	pwm2.set=0;
	pwm3.set=0;
	pwm4.set=0;
	pwm5.set=128;

	dir_clear(&dir1);
	dir_clear(&dir2);
	dir_clear(&dir3);
	dir_clear(&dir4);
	dir_clear(&dir5);

	uart1_init();
	//usart1_write("start\n\r", 7);

	wathdog_init();
	wathdog_set(50);
	clock_systick((1000/255)*4);
	
	while(true) {
		if (rtc_get() - uart1_last_message > 1000) {
			pid1.enabled=false;
			pid2.enabled=false;
			pid3.enabled=false;
			pid4.enabled=false;
			pwm5.set=0;
			dir_clear(&dir5);
		}

		if (uart1_send_message !=0 && rtc_get() - uart1_send_message > 1) {
			uart1_send_message=0;
			USART_CR1(USART3) |= USART_CR1_TXEIE;
		}

		pid_calc(&pid1);
		pid_calc(&pid2);
		pid_calc(&pid3);
		pid_calc(&pid4);
		
		wathdog_reset();
	}
	return 0;
}
