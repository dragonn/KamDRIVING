#include "crc.h"

#include <stdio.h>

uint16_t crc16_update(uint16_t crc, uint8_t a);

uint16_t crc16_update(uint16_t crc, uint8_t a) {
	int i;

	crc ^= (uint16_t)a;
	for (i = 0; i < 8; ++i) {
		if (crc & 1) {
			crc = (crc >> 1) ^ 0xA001;
        } else {
			crc = (crc >> 1);
        }
	}

	return crc;
}

uint16_t crc16_calc(uint8_t *str, uint8_t len)
{
		uint16_t crc = 0xFFFF;

		while (len>0) {
			crc = crc16_update(crc, *str);
			str++;
			len--;
		}
		return crc;
}