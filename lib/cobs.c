#include "cobs.h"

#include <stdio.h>

uint16_t cobs_encode(const uint8_t *buffer, uint16_t size, uint8_t *encoded) {
	uint16_t read_index  = 0;
	uint16_t write_index = 1;
	uint16_t code_index  = 0;
    uint8_t code       = 1;

    while (read_index < size) {
        if (buffer[read_index] == 0) {
            encoded[code_index] = code;
            code = 1;
            code_index = write_index++;
            read_index++;
        } else {
            encoded[write_index++] = buffer[read_index++];
            code++;

            if (code == 0xFF) {
                encoded[code_index] = code;
                code = 1;
                code_index = write_index++;
            }
        }
    }

    encoded[code_index] = code;
    return write_index;
}


uint16_t cobs_decode(const uint8_t *encoded, uint16_t size, uint8_t *decoded) {
    if (size == 0) {
	    return 0;
    }

    size_t read_index  = 0;
    size_t write_index = 0;
    uint8_t code       = 0;
    uint8_t i          = 0;

    while (read_index < size) {
        code = encoded[read_index];

        if (read_index + code > size && code != 1) {
            return 0;
        }

        read_index++;

        for (i = 1; i < code; i++) {
            decoded[write_index++] = encoded[read_index++];
        }

        if (code != 0xFF && read_index != size) {
            decoded[write_index++] = '\0';
        }
    }

    return write_index;
}