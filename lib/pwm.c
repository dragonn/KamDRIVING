#include <libopencm3/stm32/gpio.h>

#include "pwm.h"

volatile struct PWM pwm1;
volatile struct PWM pwm2;
volatile struct PWM pwm3;
volatile struct PWM pwm4;
volatile struct PWM pwm5;
void pwm_conf(volatile struct PWM *pwm);

void pwm_init(void) {
    pwm1.count=0;
    pwm1.set=0;
    pwm1.ports=GPIOB;
    pwm1.pin=GPIO12;

    pwm2.count=0;
    pwm2.set=0;
    pwm2.ports=GPIOB;
    pwm2.pin=GPIO13;

    pwm3.count=0;
    pwm3.set=0;
    pwm3.ports=GPIOB;
    pwm3.pin=GPIO14;

    pwm4.count=0;
    pwm4.set=0;
    pwm4.ports=GPIOB;
    pwm4.pin=GPIO15;

    pwm5.count=0;
    pwm5.set=0;
    pwm5.ports=GPIOA;
    pwm5.pin=GPIO10;

    pwm_conf(&pwm1);
    pwm_conf(&pwm2);
    pwm_conf(&pwm3);
    pwm_conf(&pwm4);
    pwm_conf(&pwm5);
}

void pwm_conf(volatile struct PWM *pwm) {
    gpio_set_mode(pwm->ports, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, pwm->pin);
}

void pwm_calc(volatile struct PWM *pwm) {
    pwm->count++;
    if (pwm->set==0) {
        gpio_clear(pwm->ports, pwm->pin);
    } else {
        if (pwm->count < PWM_MAX) {
            if (pwm->count==pwm->set) {
                gpio_clear(pwm->ports, pwm->pin);
            }
        } else {
            pwm->count = 0;
            gpio_set(pwm->ports, pwm->pin);
        }
    }
}