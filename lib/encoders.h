#ifndef ENCODERS_H
#define ENCODERS_H

#include <libopencm3/stm32/gpio.h>

void encoders_init(void);

#define ENC0_CLK_TOGGLE gpio_toggle(GPIOC, GPIO14)
#define ENC0_CLK_SET gpio_set(GPIOC, GPIO14)

#define ENC1_SSI_DATA gpio_get(GPIOB, GPIO4)
#define ENC2_SSI_DATA gpio_get(GPIOB, GPIO5)
#define ENC3_SSI_DATA gpio_get(GPIOA, GPIO2)
#define ENC4_SSI_DATA gpio_get(GPIOC, GPIO15)

#endif