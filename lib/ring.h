#ifndef RING_H
#define RING_H

//struktura buffora
typedef int32_t ring_size_t;

struct ring {
	uint8_t *data;
	volatile ring_size_t size;
	volatile uint32_t begin;
	volatile uint32_t end;
};

//rozmiar bufforów
#define BUFFER_SIZE 1024

//makra dla buffora
#define RING_SIZE(RING)  ((RING)->size - 1)
#define RING_DATA(RING)  (RING)->data
#define RING_EMPTY(RING) ((RING)->begin == (RING)->end)

//inicjalizacja bufforów dla pierwszego UART
extern void ring1_init(void);

//buffory wejściowe i wyjściowy
extern struct ring output1_ring;
extern struct ring input1_ring;

//funkcje dostępowe do bufforów
extern int32_t ring_write_ch(struct ring *ring, char ch);
extern int32_t ring_read_ch(struct ring *ring, char *ch);
extern int32_t ring_write(struct ring *ring, char *data, ring_size_t size);
extern int32_t ring_read(struct ring *ring, char *data, ring_size_t size, uint16_t timeout);
#endif