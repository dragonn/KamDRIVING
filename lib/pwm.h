#ifndef PWM_H
#define PWM_H

#include <libopencm3/stm32/gpio.h>

void pwm_init(void);
struct PWM {
   volatile int count;
   volatile int set;
   uint16_t pin;
   uint32_t ports;
};

extern volatile struct PWM pwm1;
extern volatile struct PWM pwm2;
extern volatile struct PWM pwm3;
extern volatile struct PWM pwm4;
extern volatile struct PWM pwm5;

#define PWM_MAX 255
#define PWM_MIN 0

void pwm_calc(volatile struct PWM *pwm);

#endif