#include <stdint.h>

#include <libopencm3/stm32/rtc.h>
#include "rtc.h"

void rtc_init(void) {
	rtc_awake_from_off(RCC_LSI);
	rtc_set_prescale_val(0x7fff/1000);
}

uint32_t rtc_get(void) {
	return rtc_get_counter_val();
}