#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>

#include "dir.h"

volatile struct DIR dir1;
volatile struct DIR dir2;
volatile struct DIR dir3;
volatile struct DIR dir4;
volatile struct DIR dir5;

void dir_conf(volatile struct DIR *dir);

void dir_init(void) {
    dir1.portsA=GPIOA;
    dir1.pinA=GPIO11;
    dir1.portsB=GPIOA;
    dir1.pinB=GPIO12;

    rcc_periph_clock_enable(RCC_AFIO);
    AFIO_MAPR |= AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON;
    //AFIO_MAPR |= AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_ON;
    /*gpio_set_mode(GPIOA, GPIO_MODE_INPUT, 0, GPIO15);
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, 0, GPIO3);*/
    //gpio_secondary_remap(AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_OFF);
    dir2.portsA=GPIOA;
    dir2.pinA=GPIO15;
    dir2.portsB=GPIOB;
    dir2.pinB=GPIO3;



    dir3.portsA=GPIOB;
    dir3.pinA=GPIO8;
    //nie działa - naprawiono
    dir3.portsB=GPIOB;
    dir3.pinB=GPIO9;

    dir4.portsA=GPIOB;
    dir4.pinA=GPIO0;
    //poprawiono numery portów
    dir4.portsB=GPIOA;
    dir4.pinB=GPIO5;

    dir5.portsA=GPIOA;
    dir5.pinA=GPIO4;
    dir5.portsB=GPIOA;
    dir5.pinB=GPIO3;

    dir_conf(&dir1);
    dir_conf(&dir2);
    dir_conf(&dir3);
    dir_conf(&dir4);
    dir_conf(&dir5);
}

void dir_conf(volatile struct DIR *dir) {
    gpio_set_mode(dir->portsA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, dir->pinA);
    gpio_set_mode(dir->portsB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, dir->pinB);
}

void dir_forward(volatile struct DIR *dir) {
    gpio_set(dir->portsA, dir->pinA);
    gpio_clear(dir->portsB, dir->pinB);
}

void dir_backward(volatile struct DIR *dir) {
    gpio_clear(dir->portsA, dir->pinA);
    gpio_set(dir->portsB, dir->pinB);

}

void dir_clear(volatile struct DIR *dir) {
    gpio_clear(dir->portsA, dir->pinA);
    gpio_clear(dir->portsB, dir->pinB);
}