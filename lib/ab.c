#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "ab.h"

void ab_init(void) {
    rcc_periph_clock_enable(RCC_TIM1);
	timer_set_period(TIM1, 8192);
	timer_slave_set_mode(TIM1, 0x3); // encoder
	timer_ic_set_input(TIM1, TIM_IC1, TIM_IC_IN_TI1);
	timer_ic_set_input(TIM1, TIM_IC2, TIM_IC_IN_TI2);
	timer_enable_counter(TIM1);

    rcc_periph_clock_enable(RCC_TIM2);
	timer_set_period(TIM2, 8192);
	timer_slave_set_mode(TIM2, 0x3); // encoder
	timer_ic_set_input(TIM2, TIM_IC1, TIM_IC_IN_TI1);
	timer_ic_set_input(TIM2, TIM_IC2, TIM_IC_IN_TI2);
	timer_enable_counter(TIM2);

    rcc_periph_clock_enable(RCC_TIM3);
	timer_set_period(TIM3, 8192);
	timer_slave_set_mode(TIM3, 0x3); // encoder
	timer_ic_set_input(TIM3, TIM_IC1, TIM_IC_IN_TI1);
	timer_ic_set_input(TIM3, TIM_IC2, TIM_IC_IN_TI2);
	timer_enable_counter(TIM3);
    //AFIO_MAPR |= AFIO_MAPR_TIM3_REMAP_PARTIAL_REMAP;
    //gpio_primary_remap(0, AFIO_MAPR_TIM3_REMAP_NO_REMAP);

    rcc_periph_clock_enable(RCC_TIM4);
	timer_set_period(TIM4, 8192);
	timer_slave_set_mode(TIM4, 0x3); // encoder
	timer_ic_set_input(TIM4, TIM_IC1, TIM_IC_IN_TI1);
	timer_ic_set_input(TIM4, TIM_IC2, TIM_IC_IN_TI2);
	timer_enable_counter(TIM4);

    //AFIO_MAPR_TIM4_REMAP
    //AFIO_MAPR |= AFIO_MAPR_TIM4_REMAP;
    //gpio_primary_remap(0, AFIO_MAPR_TIM4_REMAP);
} 

void ab_set(uint16_t ssi1, uint16_t ssi2, uint16_t ssi3, uint16_t ssi4) {
	timer_set_counter(TIM1, ssi1);
	timer_set_counter(TIM4, ssi2);
	timer_set_counter(TIM3, ssi3);
	timer_set_counter(TIM2, ssi4);
}