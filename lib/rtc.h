#ifndef RTC_H
#define RTC_H

extern void rtc_init(void);
extern uint32_t rtc_get(void);

#endif