#ifndef UART_H
#define UART_H
#include <libopencm3/stm32/usart.h>

#define IN_BUFFER_SIZE 64

extern void uart1_init(void);
extern int32_t usart1_write(char *data, uint32_t size);
extern int32_t usart1_read(char *data, uint32_t size, uint16_t timeout);



struct BUFFER {
    uint8_t data[IN_BUFFER_SIZE];
    uint8_t position;
};

extern volatile uint32_t uart1_last_message;
extern volatile uint32_t uart1_send_message;
#endif